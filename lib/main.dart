import 'package:flutter/material.dart';
import 'package:invest_app/home/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        title: 'Invest App',
        home: MyHomePage(),
        debugShowCheckedModeBanner: false);
  }
}
