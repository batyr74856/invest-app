class AppAssets {
  AppAssets._();

  static const String usa = 'assets/icons/usa.png';
  static const String arrow_down = 'assets/icons/arrow_down.png';
  static const String apple = 'assets/icons/apple.png';
  static const String facebook = 'assets/icons/facebook.png';
  static const String mcdonalds = 'assets/icons/mcdonalds.png';
  static const String pepsi = 'assets/icons/pepsi.png';
  static const String starbucks = 'assets/icons/starbucks.png';
  static const String twitter = 'assets/icons/twitter.png';
  static const String uber = 'assets/icons/uber.png';
  static const String whatsapp = 'assets/icons/whatsapp.png';
}
