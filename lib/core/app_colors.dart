import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color white = Colors.white;
  static const Color grey = Colors.grey;
  static const Color backgroundBlack = Color.fromRGBO(0, 0, 0, 1);
  static const Color backgroundGrey = Color.fromRGBO(14, 14, 16, 1);
  static const Color backgroundGreySecondary = Color.fromRGBO(204, 204, 204, 1);
}
