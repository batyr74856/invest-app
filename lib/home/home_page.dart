import 'package:flutter/material.dart';
import 'package:invest_app/core/app_assets.dart';
import 'package:invest_app/core/app_colors.dart';
import 'package:invest_app/home/widget/transaction_list.dart';
import 'package:invest_app/home/widget/transaction_menu.dart';
import 'package:invest_app/home/widget/transaction_title.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            TransactionTitle(),
            TransactionMenu(),
            TransactionsList(),
          ],
        ),
      ),
    );
  }
}
