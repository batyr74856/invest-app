import 'package:flutter/material.dart';

import '../../core/app_assets.dart';
import '../../core/app_colors.dart';

class TransactionMenu extends StatefulWidget {
  const TransactionMenu({Key? key}) : super(key: key);

  @override
  State<TransactionMenu> createState() => _TransactionMenuState();
}

List<DropdownMenuItem<String>> get dropdownItems {
  List<DropdownMenuItem<String>> menuItems = const [
    DropdownMenuItem(child: Text("USD Dollar"), value: "USD"),
    DropdownMenuItem(child: Text("Tenge"), value: "T"),
  ];
  return menuItems;
}

List<DropdownMenuItem<String>> get dropdownAllItems {
  List<DropdownMenuItem<String>> menuItems = const [
    DropdownMenuItem(child: Text("All"), value: "All"),
    DropdownMenuItem(child: Text("Simple"), value: "Simple"),
  ];
  return menuItems;
}

class _TransactionMenuState extends State<TransactionMenu> {
  String selectedUsd = "";
  String selectedAll = "";
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 210,
      decoration: BoxDecoration(color: AppColors.backgroundGrey),
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                Text(
                  'Transactions History',
                  style: TextStyle(color: AppColors.white, fontSize: 16),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: DropdownButtonFormField(
              dropdownColor: AppColors.backgroundGrey,
              icon: Image.asset(
                AppAssets.arrow_down,
                color: AppColors.white,
              ),
              isExpanded: true,
              hint: const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'USD Dollar',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: AppColors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              alignment: Alignment.centerLeft,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  color: AppColors.white),
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(
                      width: 0.5, color: AppColors.white.withOpacity(0.3)),
                ),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 14, horizontal: 15),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(
                      width: 0.5, color: AppColors.white.withOpacity(0.3)),
                ),
              ),
              items: dropdownItems,
              onChanged: (String? newValue) {
                setState(() {
                  selectedUsd = newValue!;
                });
              },
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                Expanded(
                  child: DropdownButtonFormField(
                    dropdownColor: AppColors.backgroundGrey,
                    icon: Image.asset(
                      AppAssets.arrow_down,
                      color: AppColors.white,
                    ),
                    isExpanded: true,
                    hint: const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'All',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                            color: AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    alignment: Alignment.centerLeft,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                        color: AppColors.white),
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                            width: 0.5,
                            color: AppColors.white.withOpacity(0.3)),
                      ),
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 14, horizontal: 15),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                            width: 0.5,
                            color: AppColors.white.withOpacity(0.3)),
                      ),
                    ),
                    items: dropdownAllItems,
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedAll = newValue!;
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 0.5,
                            color: AppColors.white.withOpacity(0.3)),
                        borderRadius: BorderRadius.circular(10)),
                    child: Icon(Icons.calendar_month_outlined,
                        color: AppColors.white.withOpacity(0.8))),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
