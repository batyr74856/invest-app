import 'package:flutter/material.dart';
import 'package:invest_app/core/app_colors.dart';

import '../../core/app_assets.dart';

class TransactionTitle extends StatelessWidget {
  const TransactionTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 60,
          decoration: BoxDecoration(color: AppColors.backgroundBlack),
        ),
        Container(
          width: double.infinity,
          height: 280,
          decoration: BoxDecoration(color: AppColors.backgroundBlack),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.arrow_back,
                      color: AppColors.white,
                    ),
                    Icon(
                      Icons.content_copy,
                      color: AppColors.white,
                    )
                  ],
                ),
              ),
              SizedBox(height: 50),
              Image.asset(
                AppAssets.usa,
                height: 50,
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 45,
                    ),
                    Text(
                      'USD Account',
                      style: TextStyle(color: AppColors.grey, fontSize: 16),
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 12),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 0.5,
                              color: AppColors.white.withOpacity(0.3)),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        'Hide',
                        style: TextStyle(color: AppColors.white),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 20),
              Text(
                '\$ 180,970.83',
                style: TextStyle(color: AppColors.white, fontSize: 22),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
