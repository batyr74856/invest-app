import 'package:flutter/material.dart';

import '../../core/app_assets.dart';
import '../../core/app_colors.dart';

class TransactionsList extends StatelessWidget {
  const TransactionsList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(color: AppColors.backgroundGreySecondary),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              'Yesterday',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.apple,
              height: 35,
            ),
            title: Text(
              'Apple',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '- \$ 55.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.facebook,
              height: 35,
            ),
            title: Text(
              'FaceBook',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '- \$ 35.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.mcdonalds,
              height: 35,
            ),
            title: Text(
              'MCDonalds',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '+ \$ 255.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.starbucks,
              height: 35,
            ),
            title: Text(
              'Starbucks',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '- \$ 105.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.pepsi,
              height: 35,
            ),
            title: Text(
              'Pepsi',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '- \$ 135.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(color: AppColors.backgroundGreySecondary),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              'Sat, December 11',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.uber,
              height: 35,
            ),
            title: Text(
              'Uber',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '16:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '+ \$ 255.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.twitter,
              height: 35,
            ),
            title: Text(
              'Twitter',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '12:23',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '+ \$ 1050.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(color: AppColors.backgroundGreySecondary),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              'Thurs, December 9',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.whatsapp,
              height: 35,
            ),
            title: Text(
              'Whatsapp',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '14:54',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '- \$ 134.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: ListTile(
            leading: Image.asset(
              AppAssets.apple,
              height: 35,
            ),
            title: Text(
              'Apple',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            subtitle: Text(
              '14:30',
              style: TextStyle(
                color: AppColors.backgroundGrey.withOpacity(0.5),
                fontSize: 10,
              ),
            ),
            trailing: Text(
              '+ \$ 1050.31 USD',
              style: TextStyle(
                  color: AppColors.backgroundBlack,
                  fontSize: 15,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
        Divider(
          color: AppColors.backgroundGrey.withOpacity(0.5),
        ),
        SizedBox(
          height: 50,
        ),
      ],
    );
  }
}
